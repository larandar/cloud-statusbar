CANONICAL_NAME=Cloud-Statusbar
TMP_DIST=/tmp/csb_dist

ARCHIVE_NAME=${CANONICAL_NAME}-$(shell git describe HEAD)-archive.tar.gz
SOURCES_NAME=${CANONICAL_NAME}-$(shell git describe HEAD)-src.tar.gz
PACKAGE_NAME=${CANONICAL_NAME}-$(shell git describe HEAD)-script

clean:
	@echo "Deleting previously generated releases"
	rm -rf dist

clean_pyc:
	@echo "Deleting pre-compiled python files"
	find . -name .DS_Store  -delete
	find . -name "*.pyc" -delete
	find . -name "__pycache__" -delete

archive: clean_pyc
	@echo "Creating an archive of the repository"
	mkdir -p dist
	rm -f "dist/${ARCHIVE_NAME}"
	tar -cz --exclude dist --exclude .git -f "dist/${ARCHIVE_NAME}" ..

src: clean_pyc
	@echo "Creating an archive of the source code"
	mkdir -p dist
	rm -f "dist/${SOURCES_NAME}"
	tar -cz --exclude dist --exclude .git -f "dist/${SOURCES_NAME}" .

package: clean_pyc
	@echo "Generating a package for easy import in iTerm2"
	mkdir -p dist
	rm -f "dist/${PACKAGE_NAME}.zip"
	(cd ../.. && zip -vr "${CANONICAL_NAME}/${CANONICAL_NAME}/dist/${PACKAGE_NAME}.zip" ${CANONICAL_NAME} --exclude @${CANONICAL_NAME}/${CANONICAL_NAME}/.exclude)


release: archive src package
