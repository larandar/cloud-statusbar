#!/usr/bin/env python3
import asyncio
import logging
from collections import namedtuple
from pathlib import Path

import iterm2
from cloud_statusbar.gcloud import GCloud_Statusbar

logging.basicConfig(
    level=logging.INFO,
    format="[%(asctime)s] {%(filename)s:%(lineno)d} %(levelname)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)


async def main(connection):
    # And get the app, it's needed for almost everything here
    app = await iterm2.async_get_app(connection)

    # Let's initialize the statusbar
    gcloud_statusbar = GCloud_Statusbar(connection, app)
    # Now we start the async polling loop
    asyncio.create_task(gcloud_statusbar.start_watch_deamon())

    # And then we can register the statusbar
    await gcloud_statusbar.register_component()


# This instructs the script to run the "main" coroutine and to keep running even after it returns.
iterm2.run_forever(main)
# iterm2.run_until_complete(main)
