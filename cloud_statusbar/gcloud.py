import asyncio
import logging
from collections import defaultdict
from configparser import ConfigParser
from pathlib import Path

import iterm2
from cloud_statusbar.helpers.change_detector import ChangeDetector

POLLING_INTERVAL = 0.25
CONFIG_DIR = Path.home() / ".config" / "gcloud"


class GCloud_Statusbar:
    @property
    def connection(self):
        return self.__connection

    @property
    def app(self):
        return self.__app

    def __init__(self, connection, app):
        self.__connection = connection
        self.__app = app
        self._has_config_changed = ChangeDetector()

        self.__component = iterm2.StatusBarComponent(
            short_description="GCloud Project Id",
            detailed_description="Show the current configured GCP project.",
            knobs=[],
            exemplar="[gcloud project]",
            update_cadence=None,
            identifier="com.larandar.cloud_statusbar.gcloud",
        )
        self.active = True

    async def register_component(self):
        await asyncio.sleep(POLLING_INTERVAL * 4)
        await self.__component.async_register(
            self.connection, GCloud_Statusbar.Coroutine
        )

    async def start_watch_deamon(self):
        while self.active:
            await self.watch_configs()
            await asyncio.sleep(POLLING_INTERVAL)

    async def watch_configs(self):
        if not (CONFIG_DIR / "active_config").exists():
            logging.warning(
                "GCP is not configured or installed, you will need to restart "
                "iTerm after installation to use the statusbar."
            )
            return

        config_changed = self._has_config_changed(CONFIG_DIR / "active_config")
        with (CONFIG_DIR / "active_config").open() as file:
            config = file.readline().strip()

        config_file = CONFIG_DIR / "configurations" / f"config_{config}"
        if config_changed or self._has_config_changed(config_file):
            current_config = ConfigParser()
            current_config.read(config_file)

            # And finally we set all variables
            await asyncio.gather(
                self.app.async_set_variable("user.gcloudConfig", config),
                self.app.async_set_variable(
                    "user.gcloudAccount",
                    current_config.get("core", "account", fallback=None),
                ),
                self.app.async_set_variable(
                    "user.gcloudProject",
                    current_config.get("core", "project", fallback=None),
                ),
            )

    @staticmethod
    @iterm2.StatusBarRPC
    async def Coroutine(
        knobs,
        config=iterm2.Reference("iterm2.user.gcloudConfig?"),
        account=iterm2.Reference("iterm2.user.gcloudAccount?"),
        project=iterm2.Reference("iterm2.user.gcloudProject?"),
    ):
        logging.info(dict(config=config, account=account, project=project))
        if all(not v for v in (account, project)):
            return "GCP not configured"
        elif config != "default":
            return [
                f"{config}: `{account or '(unset)'}`@{project or '(unset)'}",
                f"{config}: {project or '(unset)'}",
                project or "(unset)",
            ]
        else:
            return [
                f"`{account or '(unset)'}`@{project or '(unset)'}",
                project or "(unset)",
            ]
