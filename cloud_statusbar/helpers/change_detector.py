import logging
from collections import defaultdict
from dataclasses import dataclass
from pathlib import Path


@dataclass(frozen=True)
class ConfStat:
    path: Path
    size: int
    mtime: float
    ctime: float

    def FROM_PATH(path):
        stat = path.stat()
        return ConfStat(
            path=path.resolve(),
            size=stat.st_size,
            mtime=stat.st_mtime,
            ctime=stat.st_ctime,
        )


class ChangeDetector:
    def __init__(self):
        self.__statdict = defaultdict(lambda: None)

    def __call__(self, path: Path):
        stat = ConfStat.FROM_PATH(path)
        if self.__statdict[stat.path] != stat:
            logging.info("Changed from %s to %s", self.__statdict[stat.path], stat)
            self.__statdict[stat.path] = stat
            return True
        else:
            return False
