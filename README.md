# iTerm2 Cloud Statusbar

A collection of scripts and statusbar to display different informations about installed cloud services.

## What is supported

At the moment only _Google Cloud Platform_ is supported via the cli tool `gcloud`.

## Installation

1. Download the `-script.zip` from the lastest release
2. In `iTerm2` use the `Scripts > Manage > Import...` menu to select the downloaded file
   - An entry named `Cloud-Statusbar` should now be in the `Scripts` menu 
3. Click the `Scripts > Manage > Reveal Scripts in Finder` 
4. Create a folder named `AutoLaunch` if it doesn't exists
5. Move the folder `Cloud-Statusbar` into `AutoLaunch`
6. Restart `iTerm2`